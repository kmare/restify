<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Settings::all();
        return view('settings.index', compact('settings'));
    }

    public function update(Settings $settings, Request $request)
    {
        $settings->value = $request->get('value');
        $settings->save();
        return $settings;
    }
}
