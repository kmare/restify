<?php

namespace App\Http\Controllers;

use App\Settings;
use App\Table;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class TablesController extends Controller
{
    public function index()
    {
        // $tables = Table::all();
        $tables = $this->getWaitingTables();
        return $tables;
        /*return response()
            ->json(compact('tables'))
            ->header('Content-Type', 'application/json');*/
    }

    public function store(Request $request)
    {
        // TODO: check that it is unique first

        $label = $request->get('label');
        $type = $request->get('type');
        $ex = Table::where('type', '=', $type)->where('label', '=', $label)->where('updated_at', '=', null)->first();

        if ($ex != null)
            return "stop pushing the button!\n";

        // else the table with that type doesn't exist => create it
        $table = new Table();
        $table->label = $request->get('label');
        $table->type = $request->get('type');
        $table->updated_at = null;
        $table->save();

        // also update table_request
        $this->updateTableRequest("1");

        return $table;
    }

    public function update(Table $table)
    {
        $table->touch();
        return $table;
    }

    public function overview()
    {
        return view('tables.index');
    }

    public function stream()
    {
        // ob_end_clean();
        /*$response = new StreamedResponse(function() {
            while(true) {
                // echo 'data: ' . json_encode(Table::all()) . "\n\n";
                // echo 'retry: 1000\n';
                echo 'data: ' . time() . "\n\n";
                ob_end_flush();
                // ob_flush();
                flush();
                sleep(2);
            }
        });
        $response->headers->set('Content-Type', 'text/event-stream');
        // $response->headers->set('X-Accel-Buffering', 'no');
        $response->headers->set('Cache-Control', 'no-cache');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;*/
        // echo 'data: ' . json_encode(Table::all()) . "\n\n";

        // ini_set('max_execution_time', 60);
        // set_time_limit(0);
        return response()->stream(function () {
            while(true) {

                $table_request = Settings::where('key', 'table_request')->first();
                if ($table_request->value == 1) {
                    //echo 'retry: 1000' . PHP_EOL;
                    // echo 'data: ' . time() . PHP_EOL;
                    // echo 'data: ' . json_encode(Table::all()) . PHP_EOL;
                    echo 'data: ' . json_encode($this->getWaitingTables()) . PHP_EOL;
                    echo PHP_EOL;

                    $this->updateTableRequest("0");
                } else {
                    echo 'data: ' . 'NULL' . PHP_EOL;
                    echo PHP_EOL;
                }

                ob_end_flush();
                flush();

                sleep(1);
            }
        }, 200, [
            'Content-Type' => 'text/event-stream',
            'X-Accel-Buffering' => 'no',
            'Cache-Control' => 'no-cache'
        ]);
    }

    private function updateTableRequest($value)
    {
        Settings::where('key', '=', 'table_request')->update([
            'value' => $value
        ]);
    }

    private function getWaitingTables()
    {
        $waiting = Table::where('updated_at', null)->orderBy('created_at', 'ASC')->get();
        return $waiting;
    }
}
