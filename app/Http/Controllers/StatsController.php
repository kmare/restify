<?php

namespace App\Http\Controllers;

use App\Table;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{
    public function index()
    {
        return view('stats.index');
    }

    public function range($start, $end)
    {
        /*$tables = Table::all();
        $data = array();

        foreach ($tables as $table) {
            $end = Carbon::parse($table->updated_at);
            $start = Carbon::parse($table->created_at);
            // $diff = $end->diffInSeconds($start);
            $diff = $end->diffInMinutes($start);
            // array_push( $data, $diff );
            $data[] = $diff;
        }
        $coll = new Collection($data);
        // return $coll;
        return $data;*/
        // return Table::all();

        //$start = Carbon::parse($sDate);
        //$eDate = $start->addDays(7);
        // $start = $request->get('start');

        // FIXME: where updated_at not null
        $stats = DB::table('tables')
            ->selectRaw('MONTH(created_at) as month, DAY(created_at) as day, AVG(TIMESTAMPDIFF(MINUTE, created_at, updated_at)) as wait')
            ->whereBetween(DB::raw("DATE(created_at)"), [$start, $end] )
            ->whereNotNull('updated_at')
            ->groupBy(DB::raw('MONTH(created_at), DAY(created_at)'))
            ->get();

        // dd(DB::getQueryLog());

        return $stats;
    }

    public function daily($day)
    {
        // FIXME: where updated_at not null
        $dailyStats = DB::table('tables')
            ->selectRaw('HOUR(created_at) as hour, AVG(TIMESTAMPDIFF(MINUTE, created_at, updated_at)) as wait')
            ->where(DB::raw("DATE(created_at)"), $day)
            ->whereNotNull('updated_at')
            ->groupBy(DB::raw('HOUR(created_at)'))
            ->get();

        return $dailyStats;
    }

}
