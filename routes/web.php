<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/stream', 'TablesController@stream')->name('stream');

// Tables
// Route::resource('/tables', 'TablesController');
Route::get('/tables', 'TablesController@index')->name('tables.index');
Route::post('/tables', 'TablesController@store');
Route::patch('/tables/{table}', 'TablesController@update');
Route::get('/tables/overview', 'TablesController@overview')->name('tables.overview');

// Settings
Route::get('/settings', 'SettingsController@index')->name('settings.index');
Route::patch('/settings/{settings}', 'SettingsController@update');

// Stats
Route::get('/stats', 'StatsController@index')->name('stats.index');
Route::get('/stats/range/{start}/{end}', 'StatsController@range')->name('stats.range');
Route::get('/stats/daily/{day}', 'StatsController@daily')->name('stats.daily');

// Help
Route::get('/help', function() {
    return view('help.index');
})->name('help.index');

Route::get('/test/{start}', function($start) {
    return "Test!" . $start;
});

Route::post('/test/data', function (Request $request) {
    return $request->get('label'). ' - ' . $request->get('type');
});
