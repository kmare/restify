# TODO

## In progress
- choose week
- add where condition in weekly (also change name of the function)
- help?
- generate random colors for graph

## To consider
- factory pattern for tables (sort + filter)
- read status for what to send through SSE (db?)
- sort tables by date where updated_at != null - use factory model?
- delete all open rows (with a null updated_at field)
- UI to create tables
- add waitress id (maybe)
- inline update label (maybe)
- custom CSS
- color classes per type
- charts (chart.js? see laracasts)
- auth only? (will need auth for buttons pushed by clients)
- check that locale is ok (switch to DK)
- add my name, email, url to every file
- change localhost to IP
- csrf token for post data? remove? => /app/Http/Middleware/VerifyCsrfToken.php

## Done
- TablesController -> insert new table
- implement done button
- main menu -> active menu item

## Button simulation
- with curl:
```curl -d "label=VIP table 1 for Smee.&type=0" -X POST http://localhost:8000/tables```
