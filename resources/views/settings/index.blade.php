@extends('layouts.master')

@section('title')
    Settings
@endsection

@section('subtitle')
    View and modify settings
@endsection

@section('content')
    <div id="app">
        @foreach($settings as $setting)
            <div class="control">
                <div class="tag">
                    {{ $setting->group }}
                </div>
                <div class="tags has-addons">
                    <span class="tag is-dark">{{$setting->key}}</span>
                    <span class="tag is-info">{{$setting->value}}</span>
                </div>
            </div>
        @endforeach
    </div>
@endsection
