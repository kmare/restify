@extends('layouts.master')

@section('title')
    Statistics
@endsection

@section('subtitle')
    Display statistics
@endsection

@section('content')
<div id="app">
    <stats></stats>
</div>
@endsection