<footer class="footer">
    <div class="container">
        <div class="content">
            <p class="has-text-centered">
                Copyright &copy 2018 <a href="https://www.weirdloop.com" target="_blank">Weirdloop</a>
            </p>
        </div>
    </div>
</footer>
