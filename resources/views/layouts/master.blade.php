<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>

    <title>Restaurant Tables</title>
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>
</head>
<body>


<section class="hero is-info">
    <div class="hero-head">
        @include('layouts.navbar')
    </div>
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                @yield('title')
            </h1>
            <h2 class="subtitle">
                @yield('subtitle')
            </h2>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        @yield('content')
    </div>
</section>

@include('layouts.footer')

<script src="/js/app.js"></script>
</body>
</html>