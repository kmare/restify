<nav class="navbar">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{route('tables.overview')}}">
                <img src="/favicon.png" alt="Restaurant Logo">
            </a>
            <span class="navbar-burger burger" data-target="navbarMenu">
                <span></span>
                <span></span>
                <span></span>
            </span>
        </div>
        <div id="navbarMenu" class="navbar-menu">
            <div class="navbar-end">
                <div class="tabs is-right">
                    <ul>
                        <li class="{{ \Request::route()->getName() == ('tables.overview') ? 'is-active' : '' }}"><a href="{{route('tables.overview')}}">Home</a></li>
                        <li class="{{ \Request::route()->getName() == ('stats.index') ? 'is-active' : '' }}"><a href="{{route('stats.index')}}">Stats</a></li>
                        <li class="{{ \Request::route()->getName() == ('settings.index') ? 'is-active' : '' }}"><a href="{{route('settings.index')}}">Settings</a></li>
                        <li class="{{ \Request::route()->getName() == ('help.index') ? 'is-active' : '' }}"><a href="{{route('help.index')}}">Help</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
