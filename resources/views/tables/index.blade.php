@extends('layouts.master')

@section('title')
    Overview
@endsection

@section('subtitle')
    List of tables with a pending status
@endsection

@section('content')
<div id="app">
    <tables></tables>
</div>
@endsection
