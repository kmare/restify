<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create tables
        factory(\App\Table::class, 70)->create();

        // Settings defaults
        DB::table('settings')->insert([
            'key' => 'table_request',
            'value' => '0',
            'group' => 'internal',
        ]);
        DB::table('settings')->insert([
            'key' => 'host_url',
            'value' => 'http://localhost:8000',
            'group' => 'internal',
        ]);
    }
}
