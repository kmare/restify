<?php

use Faker\Generator as Faker;

$factory->define(\App\Table::class, function (Faker $faker) {

    $created = $faker->dateTimeBetween($startDate = '-2 weeks', $endDate = 'now');
    $updated = $faker->dateTimeBetween($created, $created->format('Y-m-d H:i:s') . '+4 minutes');
    return [
        'type' => $faker->numberBetween(0, 3),
        'label' => $faker->text(10),
        //'created_at' => $faker->dateTimeInInterval($startDate = '-2 weeks', $interval = '+ 4 hours'),
        'created_at' => $created,
        'updated_at' => $updated
    ];
});
