# Restaurant waiting manager

Small demo based a Laravel and VueJS where a client asks for a menu, check or is ready to order
and the server or manager is able to manage the waiting list based on the waiting time.

## Authors
<Ioannis Panteleakis><pioann@gmail.com> - [Weirdloop.com](https://www.weirdloop.com)
